data=$1
core=$2

rm -r $core/public/static
ln -s $(pwd)/$data/static $(pwd)/$core/public/static

rm -r $core/src/assets/local
ln -s $(pwd)/$data/local $(pwd)/$core/src/assets/local

rm -r $core/public/favicon.ico
ln -s $(pwd)/$data/favicon.ico $(pwd)/$core/public/favicon.ico
