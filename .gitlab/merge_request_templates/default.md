## Description

**Issues** :

## Checklist before merge :
- [ ] No remaining `console.log` 
- [ ] Version changed if new feature
- [ ] english and french translations if required


## Comment tester cette merge request ? / Comment relire ?
