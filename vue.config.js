
const webpack = require('webpack')
const localConfig = require("./src/assets/local/localconfig.json")
const templates = require('./src/assets/local/data/templates.json')['templates']
const templatesRoutes = templates.map((template) => `/edit/${template.id}`);
const path = require('path');

const SitemapPlugin = require('sitemap-webpack-plugin').default;
const PrerenderSPAPlugin  = require('prerender-spa-plugin');
const { FontsPlugin } = require('./web_plugins/fonts');
const CopyPlugin = require("copy-webpack-plugin");

const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;

const all_langs = require('./src/assets/langs.json');
const langs = all_langs.filter((lang) => localConfig['availableLangs'].indexOf(lang.code) >= 0);

const moment = require('moment')

const today = moment().format('YYYY-MM-DDThh:mm:ss');

const withoutLang = [
    { path: '/' },
    { path: '/colors' },
    { path: '/fonts' },
    { path: '/backgrounds' },
    { path: '/symbols' },
    { path: '/qrcode' },
    { path: '/edit' }
].concat(templatesRoutes.map((path) => ({ path: path })));

let paths = [];
paths = paths.concat(withoutLang.map((path) => {
    return { 'path': path.path };
}));

for (const lang of langs) {
    paths = paths.concat(withoutLang.map((path) => {
        return { 'path': '/' + lang.code + path.path };
    }));
}

const plugins = [
    new webpack.DefinePlugin({
        BUILD_UPDATE: JSON.stringify(today)
    }),
    new SitemapPlugin({ 
        "base": localConfig.url, 
        "paths": paths, 
        "options": {
            filename: 'sitemap.xml',
            lastmod: true
        }
    }),
    new FontsPlugin({ 
        fonts: require('./src/assets/local/data/fonts.json'), 
        outputFile: 'static/fonts/fonts.css',
        aktivisda: require('./package.json').version
    }),
    new CopyPlugin({
            patterns: [
                {
                    from: "./node_modules/@neslinesli93/mozjpeg-wasm/lib/mozjpeg-wasm.wasm",
                    to: "./js/mozjpeg_wasm.wasm",
                },
                // {
                //     from: "./src/plugins/compress/converters/oxipng/oxipng_bg.wasm",
                //     to: "./js/oxipng.wasm",
                // }
            ],
        options: {}
    }),
];

if (process.env.NODE_ENV === 'production') {

    plugins.push(new PrerenderSPAPlugin({
        staticDir: path.join(__dirname, 'dist'),
        registry: undefined,    
        routes: paths.map((path) => path.path),
        renderer: new Renderer({
            onlyProduction: true,
            maxConcurrentRoutes: 8,
            renderAfterDocumentEvent: 'x-app-rendered',
            renderAfterTime: 50000,
            headless: true,
            injectProperty: '__prerender',
            // Optional - Any values you'd like your app to have access to via `window.injectProperty`.
            inject: {}
        }),
        minify: {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            decodeEntities: true,
            keepClosingSlash: true,
            sortAttributes: true,
        },
        // // // TODO bug avec webpack 5 (voir https://www.npmjs.com/package/@dreysolano/prerender-spa-plugin)
        postProcess(renderedRoute) {
            // Ignore any repdirects.
            renderedRoute.route = renderedRoute.originalRoute;
            // Remove /index.html from the output path if the dir name ends with a .html file extension.
            // For example: /dist/dir/special.html/index.html -> /dist/dir/special.html
            if (renderedRoute.route.endsWith('.html')) {
                renderedRoute.outputPath = path.join(
                __dirname,
                'dist',
                renderedRoute.route
                );
            } else {
                renderedRoute.outputPath = path.join(
                __dirname,
                'dist',
                renderedRoute.route,
                'index.html'
                );
            }
            return renderedRoute;
        }
    }));
}

module.exports = {
    chainWebpack: (config) => {
        config.plugin('html')
        .tap(args => {
            args[0].title = localConfig.name;
            return args;
        });
        
        config.module
        .rule('vue')
        .use('vue-loader')
            .loader('vue-loader')
            .tap(options => {
            options.hotReload = false
            return options
        });

        config.module.rule('svgz')
            .test(/\.svgz$/)
            .use('file-loader')
            .loader('file-loader')
            .options({'name': 'img/[name].[ext]'})
            .end();
    },
    configureWebpack: {
        plugins
    },
}
