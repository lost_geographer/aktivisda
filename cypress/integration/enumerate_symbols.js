
const symbols = require('../../src/assets/local/data/symbols.json');
const path = require('path');
const fs = require('fs')

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})

// https://elaichenkov.medium.com/cypress-how-to-verify-that-file-is-downloaded-with-cy-verify-downloads-c520b7760a69
const refTemplate = 'a1a7433a-7c36-4723-bb1d-3313669b27c0'
describe('Enumerate symbols ', () => {
    symbols.sort(() => Math.random() - 0.5);

    for (let k = 0; k < symbols.length; ++k) {
        const symbol = symbols[k];
        it(`Generate template with ${k}-th symbol ${symbol.id}`, () => {
            const downloadsFolder = Cypress.config('downloadsFolder')
            const downloadedFilename = path.join(downloadsFolder, `aktivisda-${refTemplate}.png`)
            const newFilename = path.join(downloadsFolder, `symbol-${symbol.id}.png`)

            cy.task('file:exist', newFilename).then((exist) => {
                if (exist) return;

                const url = `/edit/${refTemplate}`;
                cy.visit(url);
                cy.viewport(1280, 720)
                cy.get('#new-symbol-button').click({ force: true });
                cy.wait(2000); // tmp hack for loading
                cy.get('#open-gallery-button').click({ force: true });
                cy.get(`[data-symbol-preview="${symbol.id}"]`).first().click({ force: true });
                
                cy.wait(3000); // tmp hack for loading
                cy.get('#random-palette-button').click({ force: true });
                cy.wait(3000); // tmp hack for loading
                cy.get('#export-png-button').click({ force: true, timeout: 300000 });
    
        
                cy.readFile(downloadedFilename, 'binary', { timeout: 15000 })
                    .should(buffer => expect(buffer.length).to.be.gt(100));
                
                cy.task('file:rename', { oldPath: downloadedFilename, newPath: newFilename });
            })


            
        });
    }    
})