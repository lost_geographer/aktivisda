'use strict'

import { colorSvgString, colorsChanged, loadSvg, makeHitformFunction } from '@/vida/components/svgutils.js';

import ss from '@/assets/local/data/symbols.json';
const symbols = ss['symbols'];
import bgs from '@/assets/local/data/backgrounds.json';
const backgrounds = bgs['backgrounds'];
import ImageVidaSubComponent from './imagevidasubcomponent';
import { Canvg } from 'canvg';

export default class InternalSvgVidaSubComponent extends ImageVidaSubComponent {

    static type = 'internalsvg';

    constructor() {
        super();
        this.recomputePosition = false;
        this.recomputeImage = false;
        this.type = 'internalsvg'
    }

    static randomOptions(image_use) {
        if (image_use === 'backgrounds') {
            const background = backgrounds[Math.floor(Math.random()*backgrounds.length)];
            return {
                type: this.type,
                id: background.id,
                colors: background.colors, // todo random colors
            }

        } else if (image_use === 'symbols') {
            const symbol = symbols[Math.floor(Math.random()*symbols.length)];
            return {
                type: this.type,
                id: symbol.id,
                colors: symbol.colors, // todo random colors
            }
        }
    }

    toJson() {
        return {
            type: this.type,
            id: this.id,
            colors: this.colors,
            width: this.originalSize.width,
            height: this.originalSize.height
        }
    }
    
    toPdf() {
        return {
            svg: this.coloredString,
        }
    }

    update(options) {
        return new Promise((resolve, reject) => {
            if (!options) resolve();


            if (options !== undefined && (this.id === undefined || (options.id !== undefined && this.id != options.id))) {
                let symbolUrl = '';
                // Todo fixme. I am ugly
                if (options.gallery && options.gallery == 'backgrounds') {
                    let symbolIndexOf =  backgrounds.map(e => e.id).indexOf(options.id);
                    if (symbolIndexOf < 0) {
                        console.warn(`Unknown background ${ options.id }`)
                        const background = InternalSvgVidaSubComponent.randomOptions('backgrounds');
                        this.update({ 'id': background.id }).then((x) => resolve(x));
                        return;
                    }

                    const symbolFilename = backgrounds[symbolIndexOf].filename
                    this.hitform = backgrounds[symbolIndexOf].hitform; // maybe undefined
                    symbolUrl = '/static/backgrounds/' + symbolFilename;
                    this.originalSize = { width: backgrounds[symbolIndexOf].width, height: backgrounds[symbolIndexOf].height };

                } else {
                    let symbolIndexOf =  symbols.map(e => e.id).indexOf(options.id);
                    if (symbolIndexOf < 0) {
                        console.warn(`Unknown symbol ${ options.id }`)
                        const symbol = InternalSvgVidaSubComponent.randomOptions('symbols');
                        this.update({ 'id': symbol.id }).then((x) => resolve(x));
                        return;
                    }
                    const symbolFilename = symbols[symbolIndexOf].filename
                    this.hitform = symbols[symbolIndexOf].hitform; // maybe undefined
                    symbolUrl = '/static/symbols/' + symbolFilename;
                    this.originalSize = { width: symbols[symbolIndexOf].width, height: symbols[symbolIndexOf].height };

                }
                this.colors = undefined;
                this.inPromise = true;
                loadSvg(symbolUrl).then((svgString) => {
                    this.svgString = svgString;
                    this.id = options.id;
                    this.inPromise = false;
                    this.update(options).then(() => resolve())
                        .catch((err) => reject(err));
                        
                }).catch((err) => reject(err));
                return;
            }
            
            if (options !== undefined && (!this.canvas || colorsChanged(this.colors, options.colors))) {
                this.recomputeImage = true;
                this.recomputePosition = true;
                this.colors = options.colors;
                console.assert(this.svgString);
                this.coloredString = colorSvgString(this.svgString, options.colors).string;
            }
            resolve();
        })
    }

    makeImage(imageWidth, imageHeight) {
        return new Promise((resolve, reject) => {
            if (this.inResize) reject();

            this.inResize = true;
            if(!this.canvas) this.canvas = window.document.createElement('canvas');
            const ctx = this.canvas.getContext('2d');
            this.canvgElement = Canvg.fromString(ctx, this.coloredString);
            
            this.canvgElement.resize(imageWidth, imageHeight, 'xMidYMid meet');
            this.canvgElement.render().then(() => {
                this.inResize = false;
                resolve();
            }).catch(err => reject(err));
        });
    }

    image() {
        console.assert(this.canvas);
        return this.canvas;
    }

    makeHitformFunction(ratio) {
        return makeHitformFunction(this.hitform, ratio);
    }

}