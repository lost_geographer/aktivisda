'use strict'

import ImageVidaSubComponent from './imagevidasubcomponent';

import bgs from '@/assets/local/data/backgrounds.json';
const backgrounds = bgs['backgrounds'];


export default class PhotoSubComponent extends ImageVidaSubComponent {

    static type = 'internalphoto';

    constructor(type) {
        super();
        this.type = type;
    }

    static randomOptions() {
        throw Error('Not implemented')
    }

    toJson() {
        if (this.type === 'internalphoto') {
            return {
                type: this.type,
                id: this.id,
                width: this.originalSize.width,
                height: this.originalSize.height,
            }
        } else if (this.type === 'urlphoto') {
            return {
                type: this.type,
                url: this.photoUrl,
                width: this.originalSize.width,
                height: this.originalSize.height,
            }
        } else {
            throw Error(`toJson not implemented for type ${ this.type }`)
        }
    }
    
    toPdf() {
        if (this.type === 'urlphoto') {
            return { 'image': this.photoUrl };
        }
        // todo find a way to move code from imagecomponents to here
        return { }
    }


    update(options) {
        return new Promise((resolve) => {
            if (!options) resolve();

            if (options.id !== undefined && this.id !== options.id) {
                this.photoUrl = undefined;
                this.id = options.id
                let photoIndexOf =  backgrounds.map(e => e.id).indexOf(options.id);
                const symbolFilename = backgrounds[photoIndexOf].filename
                if (photoIndexOf < 0) {
                    console.warn(`Unknown background ${options.id}`)
                    photoIndexOf = Math.floor(Math.random()*backgrounds.length);
                    const symbol = backgrounds[photoIndexOf];
                    options.id = symbol.id;
                    options = symbol;
                    this.update(options).then((x) => resolve(x));
                    return;
                }
                this.photoUrl = '/static/backgrounds/' + symbolFilename;

            } else if (options.url !== undefined && this.photoUrl !== options.url) {
                this.photoUrl = options.url;
                this.id = undefined;
            } else {
                resolve();
                return;
            }

            this.recomputeImage = true;
            if (this.photo) delete this.photo;
            this.photo = new Image();
            this.photo.onload = () => {
                this.originalSize = { width: this.photo.width, height: this.photo.height };
                resolve();
            }
            this.photo.src = this.photoUrl;
            return;
        })
    }

    makeImage() {
        return new Promise((resolve) => {
            this.inResize = false;
            resolve();
        });
    }

    image() {
        return this.photo;
    }

    makeHitformFunction() {
        return undefined;
    }
}